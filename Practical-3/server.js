var express = require('express');
bodyParser  = require('body-parser');
app         = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


//Show Route (Default)
app.get('/users', function(req, res){
    res.status(200);
    res.type('html');
    res.end('Data of users')
})

//Post Route
app.post('/users', function(req, res) {
    var username= req.body.username;
    var email= req.body.email;
    var role= req.body.role;
    var password= req.body.password;
    console.log(username);
    console.log(email);
    console.log(role);
    console.log(password);

    var msg=
    `{
        ‘Success’ :’true’, 
        ‘UserCreated’:’${username}’, 
        ‘Email’:’${email}’, 
        ‘Role’:’${role}’,
        ‘Password’:’${password}’
    }`;

    res.end(msg);
});

//Delete Route
app.delete('/user/:id', function(req,res){
    var id=req.params.id;
    res.status(200);
    var msg=
    `{
        "message": "user with id ${id} has been successfully deleted"
    }`;

    res.end(msg);

});

//Update Route
app.put('/user/:id', function(req,res){
    var id=req.params.id;
    var email= req.body.email;
    res.status(200);
    var msg=
    `{
        "message": "user with id ${id} has been successfully updated with new email ${email}"
    }`;

    res.end(msg);

});

app.listen(8080, function(){
    console.log('Server is listening')
})
